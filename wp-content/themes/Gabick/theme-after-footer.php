<?php
/**
 * Template partial used to add content to the page in Theme Builder.
 * Duplicates partial content from footer.php in order to maintain
 * backwards compatibility with child themes.
 */

$clientName = get_bloginfo('name');

?>
<?php if ( ! et_builder_is_product_tour_enabled() && ! is_page_template( 'page-template-blank.php' ) ) : ?>
    </div> <!-- #et-main-area -->
<?php endif; ?>
<div id="gabick-copyright">
    <div class="container clearfix">
        <div id="footer-copyright">
            <div class="left-side">
                © Copyright <?= date("Y"); ?> <span class="mark"><?= $clientName; ?></span>. <?= __('Tous droits réservés.','gabick-text-domain'); ?>
            </div>
            <div class="right-side">
                <?= __('Fait avec','gabick-text-domain');?> <span class="adjective">❤︎</span> <?= __('Par','gabick-text-domain');?> <a class="gabick-link" href="https://gabick.com/">Gabick</a>
            </div>
        </div>
    </div>
</div>
