<?php

/**
 * load scripts and styles on the login page
 */
add_action('login_enqueue_scripts', function() {
    wp_enqueue_style('nwayo-login', "/pub/build/styles/adminlogin.css");
});

function gabick_swapURL() {
    return home_url();
}

function gabick_loginmeta() {
    return esc_html__('Home');
}

add_filter('login_headerurl', 'gabick_swapURL');
add_filter('login_headertext', 'gabick_loginmeta');
?>
