<?php

class CustomPostTypeCreation
{
    public static function init()
    {
        $self = new self();
        add_action('init', array($self, 'createServicesPostType'));
        add_action('init', array($self, 'createMachineriesPostType'));
    }

    /* Custom Post Type Start */
    function createServicesPostType()
    {
        $supports = array(
            'title', // post title
            'editor', // post content
            'author', // post author
            'thumbnail', // featured images
            'excerpt', // post excerpt
            'custom-fields', // custom fields
            'comments', // post comments
            'revisions', // post revisions
            'post-formats', // post formats
        );

        register_post_type(
            'Services',
            // CPT Options
            array(
                'labels' => array(
                    'name' => __('Services'),
                    'singular_name' => __('Services')
                ),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'services'),
                'supports' => $supports,
            )
        );
    }

    /* Custom Post Type Start */
    function createMachineriesPostType()
    {
        $supports = array(
            'title', // post title
            'thumbnail', // featured images
            'excerpt', // post excerpt
        );

        register_post_type(
            'Machineries',
            // CPT Options
            array(
                'labels' => array(
                    'name' => __('Machineries'),
                    'singular_name' => __('Machineries')
                ),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'machineries'),
                'supports' => $supports,
            )
        );
    }
}

CustomPostTypeCreation::init();


