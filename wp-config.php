<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'precitech' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ')#w++e=42+*n_|o ]+J,L+?9ML B|Q4 ,YYF8i.?y7`1PS`JR:rfLDRSP#g.6BXR' );
define( 'SECURE_AUTH_KEY',  ',:$rFB^7p-`zuz`?d>CzRq<Pc:{zYi3)(Y;:]@uG0E43%|WDp,]&kaaWv>Gx(,n)' );
define( 'LOGGED_IN_KEY',    '}K-fiS4^2#JP(FWC{ru|KUceX.={ lw>B3pcLIY[N$#]_h<G4,>:T`u;Gyw-6G+u' );
define( 'NONCE_KEY',        'Se<qN@l!Xhef=r!<h2^mlbz%0lRcP|ZuC~Z4I ^B=-nT|9h<Y7!<wjHts8lN1Y@!' );
define( 'AUTH_SALT',        '.D!vzjl|(ZU[Jn;Cq-.AT:d6wi#%65ycHOWHdmo0NQ?xUs3H69y^M*Qn)DV`qI&!' );
define( 'SECURE_AUTH_SALT', '!=`^jm.{RG9F;Vk8iB>*V_fTnhV6-L53dda #oP(u69O[<?G/%BsQZ>.X&it6QRI' );
define( 'LOGGED_IN_SALT',   '8`I5m(8zF&=t4$0|Aj^SLYnJQq^xwjk3jPC$gnO}7F+,9Yg-5;3PEVKy~LL4wiy,' );
define( 'NONCE_SALT',       'TS<B%p@GF_$_fl,fs}7-`e%ve*R^-=8`PsMmL{YY:.V)>;#HbjKz>M,OV`bkW<oU' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
